/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"331F5398-6E01-41D6-AE83-B1820AFE9064"}
 */
var VERSION = '2.0.0'

/**
 * @private 
 * @SuppressWarnings 
 * @properties={typeid:35,uuid:"C1F63D96-E1CC-441C-937C-A7ACD1B5DB70",variableType:-4}
 */
var init = (function(){
	InvalidVersion.prototype = Object.create(Error.prototype);
	InvalidVersion.prototype.name = 'InvalidVersion';
	InvalidVersion.prototype.constructor = InvalidVersion;
})()


/**
 * @param {String} sVersion
 * 
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"39CD52AA-3481-4B9F-AC90-4A480F23778E"}
 */
function isValid(sVersion) {
	
	//https://github.com/mojombo/semver/issues/110, somewhere near the bottom
	var rxMatch = new RegExp(
	  '^' +
	  '(0|[1-9]\\d*)' + // major
	  '\\.(0|[1-9]\\d*)' + // minor
	  '\\.(0|[1-9]\\d*)' + // patch
	  '(?:-' + // start prerelease
	    '(' + // capture
	      '(?:' + // first identifier
	        '0|' + // 0, or
	        '[1-9]\\d*|' + // numeric identifier, or
	        '\\d*[a-zA-Z-][a-zA-Z0-9-]*' + // id with at least one non-number
	      ')' + // end first identifier
	      '(?:\\.' + // dot-separated
	        '(?:0|[1-9]\\d*|\\d*[a-zA-Z-][a-zA-Z0-9-]*)' + // identifier
	      ')*' + // zero or more of those
	    ')' + // end prerelease capture
	  ')?' + // prerelease is optional
	  '(?:' + // build tag (non-capturing)
	    '\\+[0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*' + // pretty much anything goes
	  ')?' + // build tag is optional
	  '$'
	)
	if (!rxMatch.test(sVersion)) {
		return false;
	}
	return true;
}

/**
 * 
 * @param {String} sVersion
 * 
 * @return {String}
 *
 * @properties={typeid:24,uuid:"8ED60B65-2ABB-4D64-AB8B-E717D209EECD"}
 */
function incrementPatch(sVersion) {
	var ver = parse(sVersion);
	ver.patch++
	return ver.toString();
}

/**
 * 
 * @param {String} sVersion
 * 
 * @return {String}
 * 
 * @properties={typeid:24,uuid:"3EEB8D34-4255-42E4-9A25-5FC8CD43355F"}
 */
function incrementMinor(sVersion) {
	var ver = parse(sVersion);
	ver.minor++;
	ver.patch = 0;
	return ver.toString();
}

/**
 * 
 * @param {String} sVersion
 * 
 * @return {String}
 * 
 * @properties={typeid:24,uuid:"A7F5CF6D-4408-4DFD-BF84-8CE7F532CCD0"}
 */
function incrementMajor(sVersion) {
	var ver = parse(sVersion);
	ver.major++;
	ver.minor = 0;
	ver.patch = 0;
	return ver.toString();
}


/**
 * Fluent interface to determine if a version matches specified conditions
 * @param {String} sVersion
 *
 * @properties={typeid:24,uuid:"39348F55-FE0C-4FD1-BE09-F718181282D5"}
 */
function is(sVersion) {
	return {
		less_than : 
			function(sTarget){
				return compare(sVersion, sTarget) === 1;
			},
		at_least : 
			function(sTarget){
				return compare(sVersion, sTarget) <= 0;
			},
		equal_to : 
			function(sTarget){
				return compare(sVersion, sTarget) === 0;
			},
		within_range : 
			function(sMin, sMax){
				return (
					compare(sVersion, sMin) <= 0 && //greater than or equal
					compare(sVersion, sMax) == 1 //less than
				);
			}
	}
}

/**
 * Fluent interface to throw an exception if a version does not match specified conditions
 * @param {String} sVersion
 * 
 * @throws {String}
 *
 * @properties={typeid:24,uuid:"B7D16AD4-DB81-4686-A910-58C6491FF7D7"}
 */
function require(sVersion) {
	return {
		
		less_than : 
			function(sTarget){
				if (!is(sVersion).less_than(sTarget)) {
					throw new InvalidVersion(utils.stringFormat("Invalid Version. Requires less than %s, got %s",[sTarget, sVersion]));
				}
				return true;
			},
		at_least : 
			function(sTarget){
				if (!is(sVersion).at_least(sTarget)) {
					throw new InvalidVersion(utils.stringFormat("Invalid Version. Requires at least %s, got %s",[sTarget, sVersion]));
				}
				return true;
			},
		equal_to : 
			function(sTarget){
				if (!is(sVersion).equal_to(sTarget)) {
					throw new InvalidVersion(utils.stringFormat("Invalid Version. Requires %s, got %s",[sTarget, sVersion]));
				}
				return true;
			},
		within_range : 
			function(sMin, sMax){
				if (!is(sVersion).within_range(sMin,sMax)) {
					throw new InvalidVersion(utils.stringFormat("Invalid Version. Requires between %s and %s, got %s",[sMin, sMax, sVersion]));
				}
				return true;
			}
	}
}

/**
 * Compare two versions
 * @param {String} sVersionA
 * @param {String} sVersionB
 * 
 * @return {Number} -1 VersionA larger, 0 equal, 1 VersionB larger
 * @properties={typeid:24,uuid:"98723F93-5D51-4C93-A080-1775E9C51AB8"}
 */
function compare(sVersionA, sVersionB) {
	
	var verA = parse(sVersionA);
	var verB = parse(sVersionB);
	
	if (!(verA && verB) ) {
		return undefined;
	}
	
	return comparePrimary(verA,verB) || comparePre(verA, verB);
}


/**
 * @typedef {{major:Number, minor:Number, patch:Number, pre:Array<String>, build:String }}
 * @private 
 * @properties={typeid:35,uuid:"473BFA4C-554F-456E-8F91-EEA40023B892",variableType:-4}
 */
var SemVer;

/**
 * Compare the primary component of a version (major.minor.patch)
 * @param {SemVer} verA
 * @param {SemVer} verB
 * @return {Number}
 * 
 * @private
 * @properties={typeid:24,uuid:"9DC21CDA-4D6E-48E2-AAD8-0E18E7C68D88"}
 */
function comparePrimary(verA, verB) {
	if (
		verA.major > verB.major ||
		(verA.major === verB.major && verA.minor > verB.minor) ||
		(verA.major === verB.major && verA.minor === verB.minor && verA.patch > verB.patch)
	) {
		return -1;
	}
	else if (
		verA.major < verB.major ||
		(verA.major === verB.major && verA.minor < verB.minor) ||
		(verA.major === verB.major && verA.minor === verB.minor && verA.patch < verB.patch)
	) {
		return 1;
	}
		
	return 0;
}

/**
 * Compare pre-release components
 * @param {SemVer} verA
 * @param {SemVer} verB
 * 
 * @private 
 *
 * @properties={typeid:24,uuid:"DDEC15A5-052F-4340-A8FE-CF0209400193"}
 */
function comparePre(verA, verB) {
	
	if (!verA.pre && !verB.pre || !verA.pre.length && !verB.pre.length) {
		return 0;
	}
	
	//Pre release is lower precedence than non-prerelease
	if (verA.pre.length && !verB.pre || verA.pre.length && !verB.pre.length) {	
		return 1
	} 
	else if (verB.pre.length && !verA.pre || verB.pre.length && !verA.pre.length) {
		return -1
	}
	
	for (var i = 0;;i++) {
		var a = verA.pre[i];
		var b = verB.pre[i];
		
		if (a === undefined && b === undefined) {
			return 0;
		}
		else if (b === undefined) {
			return -1;
		}
		else if (a === undefined) {
			return 1;
		}
		else if (a === b) {
			continue;
		}
		else {
			return compareIdentifiers(a, b);
		}
	}
	
	return 0;
}

/**
 * Compare identifiers within a pre-release string
 * @param {String} a
 * @param {String} b
 * 
 * @return {Number}
 * @private 
 *
 * @properties={typeid:24,uuid:"0DFAE926-FB4E-4CEE-B605-B99343B3098A"}
 */
function compareIdentifiers(a, b) {
	var rxNumeric = /^[0-9]+$/;
	var bIsANum = rxNumeric.test(a);
	var bIsBNum = rxNumeric.test(b);
	
	if (bIsANum && bIsBNum) {
		var nA = parseInt(a, 10);
		var nB = parseInt(b, 10);
		if (nA > nB) {
			return -1;
		}
		else if (nA < nB) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	if (!bIsANum && bIsBNum) {
		return -1;
	}
	else if (bIsANum && !bIsBNum) {
		return 1;
	}
	else if (a > b) {
		return -1;
	}
	else if (a < b) {
		return 1;
	}
	return 0;
}


/**
 * Parse a semver string into an internal representation
 * 
 * @param {String} sVersion
   @return {SemVer}
   @private 
 * @properties={typeid:24,uuid:"CD34FF93-81AE-4C5F-93FC-82C4EEDF8329"}
 */
function parse(sVersion) {
	if (!isValid(sVersion)) {
		return null;
	}
	
	var aChunk = sVersion.split('+');
	var sBuild = aChunk[1];
	
	aChunk = aChunk[0].split('-');
	var sPre = aChunk[1];
	
	aChunk = aChunk[0].split('.');
	var self = {
		major : parseInt(aChunk[0], 10),
		minor : parseInt(aChunk[1], 10),
		patch : parseInt(aChunk[2], 10),
		pre : sPre && sPre.split('.') || null,
		build : sBuild,
		toString : function() {
			return [self.major, self.minor, self.patch].join('.') + 
					(self.pre && ('-' + self.pre.join('.')) || '') +
					(self.build && ('+' + self.build) || '')
		}
	} 
	return self;
}

/**
 * @param {String} sMsg
 *
 * @properties={typeid:24,uuid:"A6B132AF-9430-47D9-AEF8-8D9BFD0831C1"}
 */
function InvalidVersion(sMsg) {
	this.message = sMsg;
}